import { Command } from 'commander';
import { generateCommand } from './src/commands/generate';

const program = new Command('Main');
program.version('0.0.1');
program.addCommand(generateCommand);

program.parse(process.argv);
