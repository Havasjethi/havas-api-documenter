import { AMainController, App, Get, Post, ResponseObj } from '../../havas-express';
import { Address } from './data';

/**
 * Hello
 * @summary Van-e sör?
 * @deprecated
 */
@AMainController
@((t: any) => {})
export class MainController extends App {
  @Get('/')
  // @ts-ignore
  greeter(@ResponseObj response: any): WeridStuff {
    response.send('asd');

    // @ts-ignore
    return;
  }

  @Get('/address')
  returnAddress(): Address {
    //@ts-ignore
    return { x: 'x', y: 'y', z: 'z' };
  }
}

interface WeridStuff {
  x: number;
  y: string;
}
