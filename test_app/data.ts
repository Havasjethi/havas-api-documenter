export class Address {
  /**
   * X coorinate of desctioption
   * useful for whatever reason
   * @deprecated
   * @type {string}
   */
  public x: string;
  public y: string;
  public c: string = 'secret';

  constructor(x: string, y: string, private z: string) {
    this.x = x;
    this.y = y;
  }

  /**
   * Some waste
   *
   * @summary Whell it is just a big mess
   * @deprecated
   * @param {[string, number, boolean, string, ...number[]]} tuple_spread_0 Where M I
   * @param {[number, string, ...number[]]} tuple_spread Salom
   * @returns {Array<any>}
   */
  uselessMethod(
    @Pdec @Pdec tuple_spread_0: [string, number, boolean, string?, ...number[]],
    @Pdec @Pdec tuple_spread: [number, string, ...number[]],
    @Pdec @Pdec _0045: [string, number],
    @Pdec _enum: EnumShot,
    @Pdec _restricted_type: YES_NO,
    @Pdec _array_indexed_object: { [x: string]: number },
    @Pdec _0011: { a: number; b: string; c: { c_inner: number } },
    @Pdec _001: {},
    @Pdec _array_of_number: Array<number>,
    @Pdec _array_number: number[],
    @Pdec _002: string,
    @Pdec @Pdec _003: String,
    @Pdec @Pdec _004: null,
    @PathShit _005: B,
    _006: C,
  ): Array<any> {
    return [];
  }

  uselessMethod2(
    xx: number | string,
    y: Function & { x: number },
    m: (x: number) => number | void | string,
  ): void {}
}

type YES_NO = 3 | 'Y';

enum EnumShot {
  X = '__X',
  Y = '__Y',
}

function Lol(target: any, method: any, desc: any) {}
function Pdec(target: any, method: any, desc: any) {}
function PathShit(target: any, method: any, desc: any) {}

class B {
  public b_property!: string;
}

export interface C {
  c_property: string;
}
