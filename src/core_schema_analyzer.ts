import { NamedType, NodeType, ObjectProperty } from 'core-types';
import * as ctjs from 'core-types-json-schema';
import { CoreTypesToOpenApiOptions } from 'core-types-json-schema/dist/lib/open-api';
import { AMainController, Controller } from 'havas-express';
import { OpenAPIV3_1 } from 'openapi-types';
import { ClassDescription, MethodDescription } from './interfaces/cls_related';

export type CoreConvertResult = OpenAPIV3_1.SchemaObject | OpenAPIV3_1.ReferenceObject | undefined;
export type CoreConvertResultCertain = OpenAPIV3_1.SchemaObject | OpenAPIV3_1.ReferenceObject;

const isSchemaObject = (cr: CoreConvertResult): cr is OpenAPIV3_1.SchemaObject =>
  cr !== undefined && (cr as OpenAPIV3_1.ReferenceObject).$ref === undefined;

export class CoreSchemaAnalyzer {
  controllerClasses: ClassDescription[] = [];
  dataClasses: ClassDescription[] = [];
  schemaComponents: OpenAPIV3_1.ComponentsObject['schemas'];

  public constructor(private infos: ClassDescription[]) {
    for (const cls of infos) {
      const isController = cls.decoratorNames.some(
        (decoratorName) => decoratorName === 'AMainController' || decoratorName === 'Controller',
      );
      (isController ? this.controllerClasses : this.dataClasses).push(cls);
    }

    const extractedTypes = this.getReferedClasses().map(
      (cls): NamedType => ({
        name: cls.name,
        type: 'object',
        properties: Object.fromEntries(
          cls.properties.map((clsProperty): [string, ObjectProperty] => [
            clsProperty.name,
            {
              node: clsProperty.type as NodeType,
              required: false,
            },
          ]),
        ),
        additionalProperties: false,
      }),
    );

    const result = ctjs.convertCoreTypesToOpenApi(
      {
        types: extractedTypes,
        version: 1,
      },
      {} as CoreTypesToOpenApiOptions,
    );

    this.schemaComponents = result.data.components!
      .schemas as OpenAPIV3_1.ComponentsObject['schemas'];
  }

  private getReferedClasses() {
    return this.dataClasses;
  }

  public getComponents(): OpenAPIV3_1.ComponentsObject {
    return { schemas: this.schemaComponents };
  }

  public getControllerMethodParameterType(
    className: string,
    methodName: string,
    parameterNameOrIndex: string | number,
  ): CoreConvertResult {
    const method = this.getControllerMethod(className, methodName);
    const parameterDescription =
      typeof parameterNameOrIndex === 'number'
        ? method.parameters[parameterNameOrIndex]
        : method.parameters.find((e) => e.name === parameterNameOrIndex);

    if (!parameterDescription) {
      throw new Error('Parameter not found!');
    }

    return this.convertNodeTypeToSchemaOjbect(parameterDescription.type)!;
  }

  public getControllerMethodReturnType(className: string, methodName: string): CoreConvertResult {
    const method = this.getControllerMethod(className, methodName);

    return this.convertNodeTypeToSchemaOjbect(method.returnType);
  }

  private getControllerMethod(className: string, methodName: string): MethodDescription {
    const matchingClasses = this.controllerClasses.filter((e) => e.name === className);

    if (matchingClasses.length !== 1) {
      throw new Error(
        `Cannot resolve Controller (${className}), found controller amount: ${matchingClasses.length}`,
      );
    }

    const method = matchingClasses[0].methods.find((e) => e.name === methodName);

    if (!method) {
      throw new Error(`Cannot resolve ${methodName} on ${className}`);
    }

    return method;
  }

  convertNodeTypeToSchemaOjbect(nodeType: NodeType): CoreConvertResult {
    let schema: CoreConvertResult;

    switch (nodeType.type) {
      case 'or':
        schema = {
          oneOf: nodeType.or.map(this.convertNodeTypeToSchemaOjbect) as CoreConvertResultCertain[],
        };
        break;
      case 'string':
        schema = {
          type: 'string',
        };
        break;
      case 'integer':
        schema = {
          type: 'integer',
        };
        break;
      case 'number':
        schema = {
          type: 'number',
        };
        break;
      case 'tuple':
        schema = {
          // @ts-ignore
          type: 'array',
          // @ts-ignore -- Open https://github.com/OAI/OpenAPI-Specification/issues/2223
          prefixItems: nodeType.elementTypes.map(this.convertNodeTypeToSchemaOjbect),
          minItems: nodeType.minItems,
          maxItems: nodeType.minItems,
        };
        break;
      case 'ref':
        schema = {
          $ref: nodeType.ref,
        };
        break;
      case 'boolean':
        schema = {
          type: 'boolean',
        };
        break;
      case 'array':
        schema = {
          type: 'array',
          items: this.convertNodeTypeToSchemaOjbect(
            nodeType.elementType,
          ) as CoreConvertResultCertain,
        };
        break;
      case 'and':
        schema = {
          allOf: nodeType.and.map(this.convertNodeTypeToSchemaOjbect) as CoreConvertResultCertain[],
        };
        break;
      case 'object':
        if (nodeType.name && this.dataClasses.some((e) => e.name === nodeType.name)) {
          schema = {
            $ref: `#/components/schemas/${nodeType.name}`,
          };
        } else {
          schema = {
            type: 'object',
            properties: Object.fromEntries(
              Object.entries(nodeType.properties!).map(([key, value]) => [
                key,
                this.convertNodeTypeToSchemaOjbect(value.node) as CoreConvertResultCertain,
              ]),
            ),
          };
        }
        break;
      case 'any':
        schema = {
          type: 'object',
        };
    }

    if (isSchemaObject(schema)) {
      schema.description = nodeType.description;
    }

    return schema;
  }
}
