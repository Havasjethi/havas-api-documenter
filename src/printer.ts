import { writeFile } from 'fs/promises';
// @ts-ignore
import * as YAML from 'json-to-pretty-yaml';

export interface PrinterConfig {
  existingFilepath: string;
  exportType: 'JSON' | 'YAML';
}

export const getPrinter = (path: string, exportType: PrinterConfig['exportType']) => {
  switch (exportType) {
    case 'YAML':
      return {
        print(data: object): Promise<void> {
          return writeFile(path, YAML.stringify(data), { encoding: 'utf8' });
        },
      };
    case 'JSON':
    default:
      return {
        print(data: object): Promise<void> {
          return writeFile(path, JSON.stringify(data, undefined, 2), { encoding: 'utf8' });
        },
      };
  }
};
