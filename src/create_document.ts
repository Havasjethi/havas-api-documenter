import { existsSync, readFileSync } from 'fs';
import { OpenAPIV3, OpenAPIV3_1 } from 'openapi-types';
import path from 'path';
import { HttpMethodType } from '../../havas-core';
import {
  ExpressCoreRoutable,
  ExpressEndpoint,
  initializeControllerTree,
  ReadType,
} from '../../havas-express';
import { CoreSchemaAnalyzer } from './core_schema_analyzer';
import { FunctionInfo, Parameter, ParameterType, ReturnTypes } from './interfaces';
import { getPrinter } from './printer';
import HttpMethods = OpenAPIV3.HttpMethods;
import ResponsesObject = OpenAPIV3_1.ResponsesObject;

interface CrestaaSD {
  filename: string;
  configuration: object;
  exportType: 'JSON' | 'YAML';
}

export class DocumentCreator {
  constructor(private clasInfo: CoreSchemaAnalyzer) {}

  createDocumentation(options: CrestaaSD) {
    const printer = getPrinter(options.filename, options.exportType);
    const controllers = initializeControllerTree(ReadType.None, true);
    const generatedSchema = this.createDocument(
      controllers,
      options.configuration as OpenAPIV3_1.Document,
    );

    const extendedSchema = Object.assign({}, options.configuration, generatedSchema);

    printer.print(extendedSchema);
  }

  private createDocument(
    controllers: ExpressCoreRoutable[],
    options: OpenAPIV3_1.Document,
  ): OpenAPIV3_1.Document {
    return {
      ...options,
      openapi: options?.openapi || '3.1.0',
      info: options?.info || { title: '', version: '' },
      paths: this.createPaths(controllers),
      components: this.clasInfo.getComponents(),
    };
  }

  private createPaths(controllers: ExpressCoreRoutable[]): OpenAPIV3_1.PathsObject {
    const endpoints = this.extractRawPathItems(controllers).sort((a, b) =>
      a.path.localeCompare(b.path),
    );
    const pathObj: OpenAPIV3_1.PathsObject = {};

    for (const endpoint of endpoints) {
      if (pathObj[endpoint.path] === undefined) {
        pathObj[endpoint.path] = {};
      }

      const operation: OpenAPIV3_1.PathItemObject['get'] = {
        description: endpoint.name,
        responses: this.endpointToResponses(endpoint),
        // deprecated: false,
        // operationId: '<? name of method ?>',
        // requestBody: undefined,
        // security: [],
        // servers: [],
        // summary: '',
        // tags: [],
      };

      const parameters = this.functionInfoToParameters(endpoint.parameters);
      if (parameters.length > 0) {
        operation.parameters = this.functionInfoToParameters(endpoint.parameters);
      }

      pathObj[endpoint.path]![endpoint.methodType] = operation;
    }

    return pathObj;
  }

  private endpointToResponses(info: FunctionInfo): ResponsesObject {
    const response: ResponsesObject = {};

    for (const rv of info.returnTypes) {
      response[rv.code] = {
        description: rv.desc,
        content: {
          [rv.mediaType]: {
            schema: rv.schema,
          },
        },
      };
    }

    return response;
  }

  private functionInfoToParameters(parameters: FunctionInfo['parameters']): any {
    const items: any[] = [];

    for (const x of parameters) {
      items.push({
        name: x.name,
        in: x.type.toString(),
        description: x.desc,
        // schema: this.schemaService.resolve(x.schema),
        schema: undefined,
        // required: false,
        // deprecated: false
      });
    }

    return items;
  }

  private extractRawPathItems(controllers: ExpressCoreRoutable[], wrapper?: any): FunctionInfo[] {
    const endpointInfos = [];

    for (const controller of controllers) {
      const subTree: FunctionInfo[] =
        controller.children.length > 0 ? this.extractRawPathItems(controller.children) : [];
      endpointInfos.push(...subTree);

      for (const [name, endpoint] of Object.entries(controller.endpoints)) {
        const info = this.extractInforamtion(endpoint, controller);
        endpointInfos.push(info);
        // const fullPath = getFullPath(controller, endpoint);
        // const endpointData =
      }
    }

    return endpointInfos;
  }

  private extractInforamtion(
    { path, parameters, methodType, methodName }: ExpressEndpoint,
    controller: ExpressCoreRoutable,
  ): FunctionInfo {
    // console.log(parameters);
    return {
      name: methodName,
      path: getFullPath(path, controller),
      methodType: this.methodTypeToHttpMethod(methodType),
      parameters: this.extractParameterInformaiton(controller, methodName),
      returnTypes: this.extractReturnType(getControllerName(controller), methodName),
    };
  }

  private methodTypeToHttpMethod(method: HttpMethodType): HttpMethods {
    switch (method) {
      case 'all':
        break;
      case 'get':
        return HttpMethods.GET;
      case 'post':
        return HttpMethods.POST;
      case 'put':
        return HttpMethods.PUT;
      case 'delete':
        return HttpMethods.DELETE;
      case 'patch':
        break;
      case 'options':
        break;
      case 'head':
        break;
    }

    return HttpMethods.GET;
  }

  private extractReturnType(className: string, methodName: string): ReturnTypes[] {
    const node = this.clasInfo.getControllerMethodReturnType(className, methodName);

    if (!node) {
      return [];
    }

    return [
      {
        code: '200',
        mediaType: 'application/json',
        desc: node.description || '',
        schema: node,
      },
    ];
  }

  /**
   * Extract info about function parameters
   * @returns {Parameter[]}
   */
  private extractParameterInformaiton(
    controller: ExpressCoreRoutable,
    methodName: string,
  ): Parameter[] {
    const items: Parameter[] = [];

    for (const parameterDecorator of controller.parameterExtractors[methodName] ?? []) {
      const inType = decoratorToParameterType(parameterDecorator.name);

      if (inType !== undefined) {
        const parameterType = this.clasInfo.getControllerMethodParameterType(
          getControllerName(controller),
          methodName,
          parameterDecorator.index!,
        );

        items.push({
          name: methodName,
          type: inType!,
          desc: parameterType!.description || '',
          schema: parameterType!,
        });
      }
    }

    return items;
  }
}

function getControllerName(controller: ExpressCoreRoutable) {
  return controller.constructor.name;
}

function decoratorToParameterType(name: string): ParameterType | undefined {
  switch (name) {
    case 'PathVariable':
      return ParameterType.Path;
    case 'Header':
      return ParameterType.Header;
    case 'Cookie':
      return ParameterType.Cookie;
    case 'Query':
      return ParameterType.Query;
  }
}

const SLASH_REDUCER = (s: string) => s.replace(/\/{2,}/g, '/');
const DYNAMIC_VARAIBLE = (s: string) => s.replace(/:(\w+)/g, '{$1}');

function getFullPath(path: string, controller: ExpressCoreRoutable) {
  // TODO :: Cache
  const parts: string[] = [path];
  let parentCtrl: ExpressCoreRoutable | undefined = controller;
  let maxIter = 100;

  do {
    parts.unshift(parentCtrl.path);
    parentCtrl = parentCtrl.parent as ExpressCoreRoutable | undefined;
  } while (parentCtrl !== undefined && maxIter-- > 0);

  return DYNAMIC_VARAIBLE(SLASH_REDUCER(parts.join('/')));
}

function createPathItem(): OpenAPIV3_1.PathItemObject {
  return {};
}
