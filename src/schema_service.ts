// import { Constructor } from 'havas-express/lib/src/util/class_decorator_util';
// import { OpenAPIV3_1 } from 'openapi-types';
//
// export type SchemaToken = string;
//
// /**
//  * Determinates wether a given Token how should be resolved (as $ref or raw schema)
//  */
// export class SchemaService {
//   private storage: { [token: string]: [base: Constructor<unknown>, count: number, computed: any] } =
//     {};
//   private limit = 100;
//   private contentItems = [];
//
//   public setContentLimit(x: number) {
//     this.limit = x;
//   }
//
//   public register(item: Constructor<unknown>): SchemaToken {
//     const name = this.resolveName(item);
//
//     (this.storage[name] ??= [item, 0, undefined])[1] += 1;
//
//     return item.name;
//   }
//
//   public resolve(token: SchemaToken): OpenAPIV3_1.SchemaObject {
//     const stored = this.storage[token];
//
//     if (!stored) {
//       throw new Error(`Unable to resovle token ${token}`);
//     } else if (!stored[2]) {
//       stored[2] = this.resolveToken(stored[0]);
//     }
//
//     return stored[2];
//   }
//
//   public getContent(): unknown {
//     return undefined;
//   }
//
//   private resolveName(item: Constructor<unknown>) {
//     return item.name;
//   }
//
//   private resolveToken(x: Constructor<unknown>): OpenAPIV3_1.SchemaObject {
//     switch (x.name) {
//       case String.name:
//         return { type: 'string' };
//       case Boolean.name:
//         return { type: 'boolean' };
//       case Number.name:
//         return { type: 'number' };
//       case Object.name: {
//         return { type: 'object' };
//       }
//     }
//
//     return {};
//   }
// }
