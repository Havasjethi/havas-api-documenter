import { createCommand } from 'commander';
import { Dirent, existsSync, readFileSync } from 'fs';
import { readdir } from 'fs/promises';
import { isAbsolute, resolve } from 'path';
import { Path } from 'typescript';
import { CoreSchemaAnalyzer } from '../core_schema_analyzer';
import { DocumentCreator } from '../create_document';
import { extractControllerInfos } from '../type_extractor';

export const generateCommand = createCommand('generate')
  .argument('<folder>', 'Base folder of controllers')
  // .option('--folder-search <parent_folder>', 'Search in given folder')
  .option('--out <filePath>', 'Output file')
  .option('--config <filePath>', 'Existing or Partial config location')
  .option('--YAML', 'Creates YAML formatted Api instead JSON')
  .description('Generate documentation')
  .action(
    async (
      folder: string,
      options: {
        YAML?: boolean;
        config?: string;
        out?: string;
      },
    ) => {
      console.log(folder, options);

      const controllerInfo: CoreSchemaAnalyzer = await createExtractor(folder);

      new DocumentCreator(controllerInfo!).createDocumentation({
        filename: options.out ?? `api.${options.YAML ? 'yaml' : 'json'}`,
        exportType: options.YAML ? 'YAML' : 'JSON',
        configuration: getExtraConfig(options.config) || {},
      });
    },
  );

const createExtractor = async (controllerFolder: string) => {
  const folders = await includeFolder(controllerFolder);

  for (const folder of folders) {
    // Read files to Activeate the Controlelr annotation,
    await import(folder);
  }

  return new CoreSchemaAnalyzer(extractControllerInfos(folders));
};

async function includeFolder(folder: string) {
  const mainFolder = resolveDirEntityPath(folder);

  const nextFolders = [mainFolder];
  const filesToImport = [];
  let maxIteration = 100;

  while (nextFolders.length > 0 && maxIteration-- > 0) {
    const results: [string, Dirent[]][] = await Promise.all(
      nextFolders
        .splice(0)
        .map(
          (folder): Promise<[string, Dirent[]]> =>
            readdir(folder, { withFileTypes: true }).then((e) => [folder, e]),
        ),
    );

    for (const [parentFolder, dirEntities] of results) {
      for (const dirEntity of dirEntities) {
        const entityPath = resolve(parentFolder, dirEntity.name);

        if (dirEntity.isFile()) {
          filesToImport.push(entityPath);
        } else if (dirEntity.isDirectory()) {
          nextFolders.push(entityPath);
        }
      }
    }
  }

  return filesToImport;
}

const resolveDirEntityPath = (filePath: string) => {
  const cFilePAthfilePath = isAbsolute(filePath) ? filePath : resolve(__dirname, filePath);

  if (!existsSync(cFilePAthfilePath)) {
    throw new Error(
      `Given folder "${cFilePAthfilePath}" dosen't exists. Absolute path: ${cFilePAthfilePath}`,
    );
  }

  return cFilePAthfilePath;
};

function getExtraConfig(filename: string | undefined): object | undefined {
  if (!filename) {
    return;
  }

  const configFilePath = resolveDirEntityPath(filename);
  const importedFile = readFileSync(configFilePath, { encoding: 'utf8' });
  return JSON.parse(importedFile);
}
