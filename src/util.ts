import { ExpressCoreRoutable } from 'havas-express';

interface TreeNode {
  path: string;
  methods: { [name: string]: Method };
  resultWrapper?: ResultWrapper;
  childrends?: TreeNode;
  parent?: TreeNode;
}

interface ResultWrapper {}

interface Method {
  name: string;
  path: string;
}

export function createTreeNode(r: ExpressCoreRoutable) {}
export function createMethod() {}
export function createResultWrapper() {}
