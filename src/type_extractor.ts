import { NamedType, NodeType, ObjectProperty } from 'core-types';
import * as ct from 'core-types';
import * as ts from 'typescript';
import { ElementFlags, ObjectType, SymbolFlags, TupleTypeReference, TypeFlags } from 'typescript';
import {
  ClassDescription,
  JsDoc,
  MethodDescription,
  PropertyDescription,
} from './interfaces/cls_related';

export class TsTypeAnalizer {
  program!: ts.Program;
  typeChecker!: ts.TypeChecker;

  constructor(private fileAbsolutePaths: string[]) {
    this.createProgramAndTypeChecker(fileAbsolutePaths);
  }

  private createProgramAndTypeChecker(fileAbsolutePaths: string[]) {
    const program = ts.createProgram(fileAbsolutePaths, {
      target: ts.ScriptTarget.ES5,
      module: ts.ModuleKind.ES2020,
      emitDecoratorMetadata: true,
      experimentalDecorators: true,
    });

    this.program = program;
    this.typeChecker = program.getTypeChecker();
  }

  public analizeAll(): ClassDescription[] {
    const classInfos: ClassDescription[] = [];

    // for (const sourceFile of this.program.getSourceFiles()) {
    for (const filePath of this.fileAbsolutePaths) {
      const sourceFile = this.program.getSourceFile(filePath);

      if (sourceFile === undefined) {
        continue;
      }

      if (sourceFile.isDeclarationFile) {
        continue;
      }

      ts.forEachChild(sourceFile, (node) => {
        // if (ts.isModuleDeclaration(node)) {
        //   // This is a namespace, visit its children
        //   ts.forEachChild(node, visit);
        // }
        if (ts.isClassDeclaration(node)) {
          const classInfo = this.extractClassInfo(node);
          classInfo.fileName = sourceFile.fileName;
          classInfos.push(classInfo);
        }
      });
    }

    return classInfos;
  }

  public static classToCore(cls: ClassDescription): NamedType {
    return {
      name: cls.name,
      type: 'object',
      properties: Object.fromEntries(
        cls.properties.map((clsProperty): [string, ObjectProperty] => [
          clsProperty.name,
          {
            node: clsProperty.type as NodeType,
            required: false,
          },
        ]),
      ),
      additionalProperties: false,
    };
  }

  private getType(s: ts.Symbol) {
    return this.typeChecker.getTypeOfSymbolAtLocation(s, s.getDeclarations()![0]);
  }

  private getJsDoc(symbol: ts.Symbol): JsDoc {
    const description = ts.displayPartsToString(symbol.getDocumentationComment(this.typeChecker));

    return {
      description: description,
      tags: symbol.getJsDocTags() || [],
    };
  }

  private extarctTypeInformation(symbol: ts.Symbol) {
    return this.transformType(
      this.typeChecker.getTypeOfSymbolAtLocation(symbol, symbol.getDeclarations()![0]),
    );
  }

  private extractClassInfo(node: ts.ClassDeclaration): ClassDescription {
    const classSymbol = this.typeChecker.getSymbolAtLocation(node.name!)!;

    const properties: PropertyDescription[] = [];
    const methods: MethodDescription[] = [];

    classSymbol.members?.forEach((symbol) => {
      const node = symbol.getDeclarations()![0];
      const isClassProperty = ts.isPropertyDeclaration(node) || ts.isParameter(node);

      if (isClassProperty) {
        const typeInfo = this.extarctTypeInformation(symbol) as ct.NodeType;
        const name = symbol.getName();
        const description = ts.displayPartsToString(
          symbol.getDocumentationComment(this.typeChecker),
        );

        typeInfo.name = name;
        typeInfo.description = description;

        const y: PropertyDescription = {
          name: symbol.getName(),
          type: typeInfo,
          docs: {
            tags: symbol.getJsDocTags(this.typeChecker),
            deprecated: symbol.getJsDocTags(this.typeChecker).some((e) => e.name === 'deprecated'),
            description: '',
          },
        };
        properties.push(y);
      }

      if (ts.isMethodSignature(node) || ts.isMethodDeclaration(node)) {
        methods.push(this.extractMethodDescription(symbol, classSymbol));
      }
    });

    const decoratorNames: string[] = node.decorators
      ?.map((e) =>
        (
          this.typeChecker.getSymbolAtLocation(e.expression) ||
          this.typeChecker.getSymbolAtLocation((e.expression as ts.CallExpression).expression)
        )?.getName(),
      )
      ?.filter((decoratorSymbol) => decoratorSymbol !== undefined) as string[];

    return {
      name: classSymbol.getName(),
      methods,
      properties,
      decoratorNames: decoratorNames || [],
    };
  }

  private extractMethodDescription(
    methodSymbol: ts.Symbol,
    classSymbol: ts.Symbol,
  ): MethodDescription {
    const callSignature = this.typeChecker
      .getTypeOfSymbolAtLocation(methodSymbol, classSymbol.valueDeclaration!)
      .getCallSignatures()[0];

    const parameters = callSignature.getParameters().map((parameterSymbol, index) => {
      return {
        index,
        name: parameterSymbol.getName(),
        type: this.getParameterType(parameterSymbol),
      };
    });

    const returnType = this.transformType(this.typeChecker.getReturnTypeOfSignature(callSignature));
    const docs = this.getJsDoc(methodSymbol);
    const summary = ts.displayPartsToString(docs.tags.find((e) => e.name === 'summary')?.text);

    return {
      parameters,
      returnType,
      name: methodSymbol.getName(),
      docs: {
        description: docs.description,
        tags: methodSymbol.getJsDocTags(this.typeChecker),
        summary,
      },
    };
  }

  private getParameterType(symbol: ts.Symbol): ct.NodeType {
    const rawType = this.getType(symbol);
    const transformedType = this.transformType(rawType) as ct.NodeType;
    const docs = this.getJsDoc(symbol);

    transformedType.name = symbol.getName();

    if (docs.description) {
      transformedType.description = docs.description;
    }

    return transformedType;
  }

  private transformType(type: ts.Type): ct.NodeType {
    const isUnion = type?.isUnion() || false;
    const isIntersection = type?.isIntersection() || false;
    const isClassOrInterface = type?.isClassOrInterface() || false;
    const isClass = type?.isClass() || false;
    const typeFlag = type.getFlags();

    const isString = isFlagActive(typeFlag, TypeFlags.String);
    const isNumber = isFlagActive(typeFlag, TypeFlags.Number);
    const isBoolean = isFlagActive(typeFlag, TypeFlags.Boolean);
    const isAny = isFlagActive(typeFlag, TypeFlags.Any);
    const isUnkown = isFlagActive(typeFlag, TypeFlags.Unknown);
    const isNull = isFlagActive(typeFlag, TypeFlags.Null);
    const isObject = isFlagActive(typeFlag, TypeFlags.Object);
    const isVoid = isFlagActive(typeFlag, TypeFlags.VoidLike);

    let nodeType: ct.NodeType | undefined;

    if (isString) {
      nodeType = ct.simplify({
        type: 'string',
      });
    } else if (isNumber) {
      nodeType = ct.simplify({
        type: 'number',
      });
    } else if (isBoolean) {
      nodeType = ct.simplify({
        type: 'boolean',
      });
    } else if (isAny || isUnkown) {
      nodeType = ct.simplify({
        type: 'any',
      });
    } else if (isVoid || isNull) {
      nodeType = ct.simplify({
        type: 'null',
      });
    } else if (isClassOrInterface && !isClass) {
      // const typeSymbol = type.getSymbol();
      //
      // // Booelan, Number, String classes
      // switch (typeSymbol?.getName()) {
      //   case String.name:
      //     nodeType = ct.simplify({
      //       type: 'string',
      //     });
      //     break;
      //   case Number.name:
      //     nodeType = ct.simplify({
      //       type: 'number',
      //     });
      //     break;
      //   case Boolean.name:
      //     nodeType = ct.simplify({
      //       type: 'boolean',
      //     });
      //     break;
      // }
      //
      // // Interface
      // if (
      //   !nodeType &&
      //   typeSymbol?.getFlags() &&
      //   isFlagActive(typeSymbol?.getFlags(), SymbolFlags.Interface)
      // ) {
      //   nodeType = this.extractClassLike(typeSymbol);
      // }
    } else if (isClass) {
      // Class
      nodeType = TsTypeAnalizer.classToCore(
        this.extractClassInfo(type.getSymbol()?.valueDeclaration! as ts.ClassDeclaration),
      );
    } else if (isUnion) {
      nodeType = ct.simplify({
        type: 'or',
        //@ts-ignore
        or: type.types.map((e) => this.transformType(e)),
      });
    } else if (isIntersection) {
      nodeType = ct.simplify({
        type: 'and',
        //@ts-ignore
        and: type.types.map((e) => this.transformType(e)),
      });
    } else if (isObject) {
      //
      nodeType = this.objectToCoreType(type as ObjectType);
    } else if (isFlagActive(typeFlag, ts.TypeFlags.StringLiteral)) {
      nodeType = ct.simplify({
        type: 'string',
        const: (type as ts.StringLiteralType).value,
      });
    } else if (isFlagActive(typeFlag, ts.TypeFlags.NumberLiteral)) {
      nodeType = ct.simplify({
        type: 'number',
        const: (type as ts.NumberLiteralType).value,
      });
    } else {
      const unknown = 123;
    }

    return (
      nodeType! ||
      ct.simplify({
        type: 'any',
      })
    );
  }

  private objectToCoreType(type: ts.ObjectType): ct.NodeType | undefined {
    const typeChecker = this.typeChecker;
    const objectFlag = (type as ObjectType).objectFlags;
    const typeSymbol = type.getSymbol();

    // @ts-ignore
    if (typeSymbol && ts.symbolName(typeSymbol) === 'Array' && type.resolvedTypeArguments) {
      return {
        type: 'array',
        // @ts-ignore
        elementType: this.transformType(type.resolvedTypeArguments[0]),
      };
    } else if (typeSymbol && ts.symbolName(typeSymbol) === '__type') {
      return this.extractClassLike(typeSymbol);
    } else if (isFlagActive(objectFlag, ts.ObjectFlags.Reference)) {
      (type as TupleTypeReference).target.hasRestElement;
      // @ts-ignore
      const resolvedTypeArguments: ts.Type[] = type.resolvedTypeArguments;
      let additionalItems: false | NodeType = false;

      return ct.simplify({
        type: 'tuple',
        elementTypes: resolvedTypeArguments.map((subElement, index) => {
          const x = (type as TupleTypeReference).target.elementFlags[index];
          const resolvedType = this.transformType(subElement) as NodeType;
          if (isFlagActive(x, ElementFlags.Rest)) {
            additionalItems = resolvedType;
          }
          return resolvedType;
        }),
        minItems: (type as TupleTypeReference).target.minLength || resolvedTypeArguments.length,
        additionalItems,
      });
    }

    console.error('Item Object cannot be identified');
  }

  private extractClassLike(typeSymbol: ts.Symbol): ct.NodeType {
    const members: any = {};
    const iter = typeSymbol.members?.entries()!;
    let additionalProperties = false;

    let item: ReturnType<typeof iter['next']>;
    let maxIter = 1_000;

    item = iter.next();
    while (!item.done && maxIter-- > 0) {
      const [key, value] = item.value!;
      if (key === '__index') {
        additionalProperties = true;
        continue;
      }
      const extractedType = this.transformType(
        this.typeChecker.getTypeOfSymbolAtLocation(value, value.valueDeclaration!),
      );

      members[key.toString()] = {
        node: extractedType,
        required: true,
      };
      item = iter.next();
    }

    // Note:: Dangerous!!
    return ct.simplify({
      type: 'object',
      properties: members,
      additionalProperties,
    });
  }
}

const isFlagActive = (inputFlag: number, checkedFlag: number): boolean =>
  (inputFlag | checkedFlag) === inputFlag || (inputFlag | checkedFlag) === checkedFlag;

export function extractControllerInfos(fileAbsolutePaths: string[]): ClassDescription[] {
  const typeAnalizer = new TsTypeAnalizer(fileAbsolutePaths);

  return typeAnalizer.analizeAll();
}
