import { JSDocTagInfo } from 'typescript';
import { NodeType } from 'core-types';

type ReturnType = NodeType;

interface JsDocable<T extends JsDoc> {
  docs: T;
}

export interface JsDoc {
  description: string;
  tags: JSDocTagInfo[];
}

interface PropertyDoc extends JsDoc {
  deprecated: boolean;
}

interface MethodDoc extends JsDoc {
  summary: string;
}

export interface ClassDescription {
  fileName?: string;
  name: string;
  // extended_class?: ClassExtender;
  // decorators: DecoratorDescription[];
  properties: PropertyDescription[];
  methods: MethodDescription[];
  decoratorNames: string[];
}

export interface PropertyDescription extends JsDocable<PropertyDoc> {
  name: string;
  type: NodeType;
  // required?
  //  allowEmptyValue

  // initializerValue: boolean;
  // decorators?: DecoratorDescription[];
}

export interface MethodDescription extends JsDocable<MethodDoc> {
  name: string;
  // decorators?: DecoratorDescription[];
  parameters: ParameterDescription[];
  returnType: ReturnType;
}

export interface ParameterDescription {
  name: string;
  type: ReturnType;
  index: number;
  // default_value?: any;
}
