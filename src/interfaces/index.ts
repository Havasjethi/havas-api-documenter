import { OpenAPIV3_1 } from 'openapi-types';

export interface FunctionInfo {
  name: string;
  path: string;
  methodType: OpenAPIV3_1.HttpMethods;
  parameters: Parameter[];
  returnTypes: ReturnTypes[];
}

export interface ReturnTypes {
  code: string;
  desc: string;
  mediaType: string;
  schema: OpenAPIV3_1.SchemaObject;
}

export interface Parameter {
  name: string;
  type: ParameterType;
  schema: OpenAPIV3_1.SchemaObject;
  desc?: string;
  required?: boolean;
}

export enum ParameterType {
  Query = 'query',
  Path = 'path',
  Header = 'header',
  Cookie = 'cookie',
}
